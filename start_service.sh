
#########################################################
##### start indivual ###################################
#########################################################
#example


#  ./start_service.sh postgres
#  ./start_service.sh pgadmin
#  ./start_service.sh airflow



## pass the service name used in the docker compose
service=$1

: "${service:="airflow"}"

services=['pgadmin','postgres','airflow']

if [[ "${services[*]}" == *"$service"* ]]
then
  echo
  echo "Preparing to run the service: $service"
  echo ...
else
  echo "service: $service not created yet"
  exit 1
fi


## gettinting the full path
current_dir=$(pwd)

echo building the image
docker build -t $service -f ./dockers/$service/docker/${service^}.Dockerfile ./dockers/$service/docker

echo
echo "creating the network"
echo
docker network inspect home-compose-network >/dev/null 2>&1 || \
docker network create --driver bridge home-compose-network

echo
echo "removing the container if exists"
docker stop $service || true && docker rm $service || true
echo


file_install_data=./dockers/$service/docker/scripts/install_share_data.sh
#checking if the file already exists
if [ -f "$file_install_data" ]; 
then

  ################## config for data share ########
   echo "installing the file: $file_install_data"
   echo ...
   sudo chmod +x $file_install_data
   $file_install_data $current_dir $service

else
   echo "No data share to install for the service: $service"

fi


echo
echo "lauching the container"
echo ...
if [ "${service}" == "pgadmin" ];
then

    docker run -d \
   -e PGADMIN_SETUP_EMAIL=email@hotmail.com \
   -e PGADMIN_SETUP_PASSWORD=$service \
   -p 5050:5050 \
   --name $service \
   -h $service \
   -u $service \
   --network home-compose-network \
   -v $current_dir/dockers/$service/data:/var/lib/pgadmin \
   $service

elif [ "${service}" == "postgres" ];
then

    docker run -d \
   -p 15432:5432 \
   --name $service \
   -h $service \
   -u $service \
   --network home-compose-network \
   -v $current_dir/dockers/$service/data:/var/lib/pgsql/data \
   $service


elif [ "${service}" == "airflow" ];
then
     docker run -d \
   -e LOAD_EX=yes \
   -e FERNET_KEY=46BKJoQYlPPOexq0OhDZnIlNepKFf87WFwLbfzqDDho= \
   -e EXECUTOR=LocalExecutor \
   -e POSTGRES_USER=airflow \
   -e POSTGRES_PASSWORD=airflow \
   -e POSTGRES_DB=airflow \
   -e POSTGRES_PORT=5432 \
   -e POSTGRES_HOST=postgres \
   -p 8083:8080 \
   --name $service \
   -h $service \
   -u $service \
   --network home-compose-network \
   -v $current_dir/dags:/opt/airflow/dags \
   -v $current_dir/config/airflow.cfg:/opt/airflow/airflow.cfg \
   $service

else
  echo "service not created yet"
  exit 1
fi


echo
#echo entring in the container as root
echo
#docker exec -it --user root $service /bin/bash



