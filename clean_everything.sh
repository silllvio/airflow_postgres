# /!\ WARNING: RESET EVERYTHING! 
# Remove all containers/networks/volumes/images and data in db
docker kill $(docker ps -q)
docker system prune -f
docker volume prune -f
docker network prune -f
docker rmi -f $(docker images -a -q)