#!/bin/bash

## gettinting the full path
current_dir=$(pwd)

# Prepare the datashare
services=('pgadmin' 'postgres' )
for service in "${services[@]}"
do
   file_install_data=./dockers/$service/docker/scripts/install_share_data.sh
   echo
   echo 
   echo "Configuring the system to share files with the container: $service"
   echo "Running the configfile: $file_install_data"
   echo ...
   chmod +x $file_install_data
   $file_install_data $current_dir $service
   echo
   echo
done


# Startup all the containers at once
docker-compose -f dockers/airflow_postgres.yml up -d
