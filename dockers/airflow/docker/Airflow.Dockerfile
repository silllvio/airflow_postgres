# VERSION 0.0.0
# AUTHOR: Silvio Liborio

#Get the docker image from hub - Official Centos7
FROM centos:7
LABEL maintainer="Centos"

# Arguments that can be set with docker build
ARG AIRFLOW_HOME=/opt/airflow

# Export the environment variable AIRFLOW_HOME where airflow will be installed and aux
ENV AIRFLOW_HOME=${AIRFLOW_HOME}


##############################################################################
################# Creating and Managing the user Airflow #####################
##############################################################################
RUN useradd --shell /bin/bash --create-home --home $AIRFLOW_HOME airflow
RUN echo "airflow" | passwd --stdin airflow
RUN usermod -aG wheel airflow


#########################################################
##### creating the directories #########################
#########################################################
RUN    mkdir $AIRFLOW_HOME/logs \
    && mkdir $AIRFLOW_HOME/dags \
    && mkdir $AIRFLOW_HOME/on_init \
    && mkdir $AIRFLOW_HOME/tmp \
    && mkdir $AIRFLOW_HOME/plugins
 
 # Set workdir (it's like a cd inside the container)
WORKDIR ${AIRFLOW_HOME}


##############################################################################
################# preparing the artifacts ####################################
##############################################################################

# Copy the entrypoint.sh from host to container (at path AIRFLOW_HOME)
COPY scripts/entrypoint.sh $AIRFLOW_HOME/on_init/entrypoint.sh
RUN echo "file imported" && \ 
    cat $AIRFLOW_HOME/on_init/entrypoint.sh

# Set the entrypoint.sh file to be executable 
RUN chmod +x $AIRFLOW_HOME/on_init/entrypoint.sh 

#wget https://raw.githubusercontent.com/apache/airflow/constraints-1.10.12/constraints-3.6.txt -O $AIRFLOW_HOME/tmp/constraints-3.6.txt 
COPY config/constraints-3.6.txt $AIRFLOW_HOME/tmp/constraints-3.6.txt

COPY config/airflow.cfg ${AIRFLOW_USER_HOME}/airflow.cfg

RUN chown airflow:airflow $AIRFLOW_HOME -R && \
    chmod 755 $AIRFLOW_HOME -R

##############################################################################
################# Starting the install of the environment ####################
##############################################################################


## install Core dependencies and basic tools of the Centos
RUN yum -y install deltarpm
RUN yum -y install epel-release && \
    rpm -U http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm 
RUN yum -y update && \
    yum clean all && \
    yum -y install \
        wget \
        git \
        gcc \
        net-tools \
        bzip2-devel \
        expat-devel \
        gdbm-devel \
        ncurses-devel \
        openssl-devel \
        readline-devel \
        sqlite-devel \
        tk-devel \
        xz-devel \
        zlib-devel \
        libffi-devel \
        nmap-ncat \
        krb5-devel \
        cyrus-sasl \
        cyrus-sasl-devel \
        cyrus-sasl-gs2 \
        cyrus-sasl-gssapi \
        cyrus-sasl-lib \
        cyrus-sasl-md5 \
        cyrus-sasl-plain \
        krb5-workstation \
        which \
        cronie-noanacron \
        python3 \
        python3-devel \
        sudo && \
    yum -y groupinstall --disablerepo=\* --enablerepo=base,updates,cr "Development Tools" && \
    pip3 install --upgrade pip


##############################################################################
################# Install the Airflow and dependencies #######################
##############################################################################

RUN pip3 install wheel psycopg2-binary pytz cryptography pyOpenSSL ndg-httpsclient pyasn1 celery configparser && \
    pip3 install --upgrade setuptools && \
    pip3 install --upgrade backports.ssl-match-hostname && \
    pip3 install "apache-airflow[crypto,celery,postgres,kubernetes,docker]"==1.10.12 --constraint $AIRFLOW_HOME/tmp/constraints-3.6.txt


# Changing all the the user Airflow and cleaning the tmp 
RUN chown airflow:airflow $AIRFLOW_HOME -R && \
    chmod 755 $AIRFLOW_HOME -R && \
    rm -f -R $AIRFLOW_HOME/tmp/*

# Expose internal ports to the others containers
EXPOSE 8080 5555 8793 22

USER airflow

# Run on init the airflow
# Execute the entrypoint.sh
ENTRYPOINT [ "/opt/airflow/on_init/entrypoint.sh" ]



##############################################################################
##### Helper commands ###################################
##############################################################################

#build the image
# docker build -t airflow -f ./airflow/docker/Airflow.dockerfile ./airflow/docker

#run the image
##run the container

# docker run -it --name test_airflow \
# -p 8085:8080 -p 2230:22 -u root -h test_airflow \
# --network airflowpostgres_default \
# -e EXECUTOR=LocalExecutor \
# -e POSTGRES_USER=airflow \
# -e POSTGRES_PASSWORD=airflow \
# -e POSTGRES_DB=airflow \
# -e POSTGRES_PORT=5432 \
# -e POSTGRES_HOST=postgres \
# --entrypoint /bin/bash  airflowpostgres_airflow_local:latest


#  docker exec -i -t --user root test_airflow /bin/bash
