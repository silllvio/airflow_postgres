#!/usr/bin/env bash


##############################################################################
################# felling variables if it's empty ############################
##############################################################################
: "${LOAD_EX:="n"}"
: "${EXECUTOR:="SequentialExecutor"}"
: "${AIRFLOW_HOME:="/opt/airflow"}"
: "${POSTGRES_HOST:="postgres"}"
: "${AIRFLOW__CORE__FERNET_KEY:=${FERNET_KEY:=$(python -c "from cryptography.fernet import Fernet; FERNET_KEY = Fernet.generate_key().decode(); print(FERNET_KEY)")}}"



# Load DAGs exemples (default: yes, but with this script the default is not)
if [ "${LOAD_EX}" == "n" ];
then
   AIRFLOW__CORE__LOAD_EXAMPLES=False
else
   AIRFLOW__CORE__LOAD_EXAMPLES=True
fi


if [ "${EXECUTOR}" != "SequentialExecutor" ]; 
then
  AIRFLOW__CORE__SQL_ALCHEMY_CONN="postgresql+psycopg2://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB"
  AIRFLOW__CELERY__RESULT_BACKEND="db+postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB"
  AIRFLOW__CORE__EXECUTOR="${EXECUTOR}"
fi


export \
  AIRFLOW_HOME \
  AIRFLOW__CORE__LOAD_EXAMPLES \
  AIRFLOW__CORE__SQL_ALCHEMY_CONN \
  AIRFLOW__CELERY__RESULT_BACKEND \
  AIRFLOW__CORE__EXECUTOR \
  AIRFLOW__CORE__FERNET_KEY


# Install custom python package if requirements.txt is present
if [ -e ${AIRFLOW_HOME}/dags/requirements.txt ]; then
    $(command -v pip3) install --user -r $AIRFLOW_HOME/dags/requirements.txt
fi

# Initiliase the metastore
airflow initdb

# Run the scheduler in background
airflow scheduler &> /dev/null &

# Run the web server in foreground (for docker logs)
exec airflow webserver