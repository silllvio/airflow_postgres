
v_home=$1
v_service=$2

: "${v_home:="../../../.."}"
: "${v_service:="pgadmin"}"


dir_data=$v_home/dockers/$v_service/data

#checking if the file already exists
if [ -d "$dir_data" ]; 
then
  echo "Data folder share alread exists: $HOME_TMP_EX_DT/dockers/$v_service/data"
  echo "Drop it manually to create a new"
  echo "For now it'll be used the the old data"
else

    echo "creating the directory $dir_data"

    mkdir -p $dir_data
    sudo chown -R 1000:50 $dir_data

fi