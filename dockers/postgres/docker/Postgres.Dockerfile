# VERSION 0.0.0
# AUTHOR: Silvio Liborio

#Get the docker image from hub - Official Centos7
FROM centos:7
LABEL maintainer="Centos"

#Setting the variables
ARG PG_DIR=/var/lib/pgsql
ENV PG_DIR=${PG_DIR}


#########################################################
#########################################################
### Installing the postgres and updating tolls 
#########################################################
RUN yum -y update && \
    yum clean all && \
    yum -y install postgresql-server \
    postgresql-contrib


#########################################################
#########################################################
### creating the directories 
#########################################################
RUN mkdir $PG_DIR/tmp && \
    mkdir $PG_DIR/on_init 


#########################################################
#########################################################
### Creating the artifacts 
#########################################################

COPY ./scripts/config_init.sql $PG_DIR/tmp/config_init.sql
RUN echo "file imported" && \ 
    cat $PG_DIR/tmp/config_init.sql

# Set the entrypoint.sh file to be executable 
RUN chmod +x $PG_DIR/tmp/config_init.sql

RUN chown postgres:postgres $PG_DIR -R && \
    chmod 0700 $PG_DIR -R


#########################################################
#########################################################
### Starting and configuring the server
#########################################################
USER postgres

RUN /bin/initdb -D /var/lib/pgsql/data

# Starting the server -D datadir -S silent, oline error -O options -W waint the server start -T timeout 300
RUN /usr/bin/pg_ctl start -D /var/lib/pgsql/data -s -o "-p 5432" -w -t 300 &&\
                /bin/psql -f $PG_DIR/tmp/config_init.sql 

RUN echo "host all  all    0.0.0.0/0  md5" >> /var/lib/pgsql/data/pg_hba.conf

RUN echo "listen_addresses='*'" >> /var/lib/pgsql/data/postgresql.conf

EXPOSE 5432 22

VOLUME ["/var/lib/pgsql/data"]

CMD ["/bin/postgres", "-D", "/var/lib/pgsql/data", "-c", "config_file=/var/lib/pgsql/data/postgresql.conf"]

#########################################################
##### Helper commands ###################################
#########################################################

#build the image
# docker build -t postgres -f ./docker/Postgres.Dockerfile ./docker

#  docker run -it --name postgres -p 2223:22 -p 15432:5432 -h postgres -v data:/var/lib/pgsql/data --network postgres_postgres-compose-network -u postgres -e POSTGRES_PASSWORD=postgres postgres
#  docker cp postgres:/var/lib/pgsql/data .

#  docker run -it --name postgres -p 2223:22 -p 15432:5432 -h postgres -v /home/silvio/silvio/dockers/postgres/data:/var/lib/pgsql/data -u postgres -e POSTGRES_PASSWORD=postgres postgres

#  docker run -it --name postgres -p 2223:22 -p 15432:5432 -h postgres -u postgres -e POSTGRES_PASSWORD=postgres postgres


#enter in the image
# docker start postgres
#  docker exec -it --user postgres postgres /bin/bash
#  docker exec -it --user root postgres /bin/bash
