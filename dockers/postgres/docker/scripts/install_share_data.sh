HOME_TMP_EX_DT=$1
v_service=$2

: "${HOME_TMP_EX_DT:="../../../.."}"
: "${v_service:="postgres"}"

#checking if the file already exists
if [ -d "$HOME_TMP_EX_DT/dockers/$v_service/data" ]; 
then
  echo "Data folder share alread exists: $HOME_TMP_EX_DT/dockers/$v_service/data"
  echo "Drop it manually to create a new"
  echo "For now it'll be used the the old data"
else

    echo 'Criating the variables'
    TMP_CONTAINER='containertmp42903840248'
    TMP_IMAGE='imagetmp42903840248'

    echo 'Creating the user postgres'
    sudo adduser postgres
    echo "postgres" | passwd --stdin postgres
    sudo usermod -aG wheel postgres
    sudo groupadd Postgres
    sudo usermod -aG Postgres $USER

    echo 'Removing the TMP'
    docker stop -f $TMP_CONTAINER
    docker rm $TMP_CONTAINER
    docker image rmi -f $TMP_IMAGE

    echo 'Creating the image'
    docker build -t $TMP_IMAGE -f $HOME_TMP_EX_DT/dockers/$v_service/docker/Postgres.Dockerfile $HOME_TMP_EX_DT/dockers/$v_service/docker

    echo 'Starting the container'
    docker run -d --name $TMP_CONTAINER -u postgres -e POSTGRES_PASSWORD=postgres -t $TMP_IMAGE

    echo 'Cleaning the directory ${HOME_TMP_EX_DT}/data'
    sudo rm -f -R $HOME_TMP_EX_DT/data

    echo 'Coping the data from to container to host'
    docker cp $TMP_CONTAINER:/var/lib/pgsql/data $HOME_TMP_EX_DT/dockers/$v_service

    echo 'Changing the owner of the files'
    sudo chown postgres:postgres $HOME_TMP_EX_DT/dockers/$v_service/data -R 
    sudo chmod 0700 $HOME_TMP_EX_DT/dockers/$v_service/data -R

    echo 'cleaning all'
    docker stop $TMP_CONTAINER
    docker rm $TMP_CONTAINER
    docker image rmi -f $TMP_IMAGE

fi




