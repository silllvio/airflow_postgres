DO LANGUAGE plpgsql
$body$
DECLARE
    v_user varchar;
	rec_user record;
	v_query varchar;
	v_database varChar;
BEGIN
		raise notice 'Task 01 - creating the users';
		
		Drop Table if exists tb_users;
		create temporary table tb_users as select s_user from (select 'admin' s_user union all select 'airflow' s_user)a;
		
		For rec_user in select * from tb_Users
		  Loop
			v_user := Null;
			SELECT usename FROM pg_user WHERE usename = rec_user.s_user into v_user;
			if v_user is null
			then
			raise notice 'user not founded! A new user with name: % it will be created', rec_user.s_user;
					v_query := 'CREATE USER ' || rec_user.s_user || ' WITH SUPERUSER PASSWORD '|| '''' || rec_user.s_user || '''' ;
					--raise notice '%', v_query ;
					execute v_query;
					
			else
					raise notice 'user: % already exists! Skipping!', rec_user.s_user;
			end if;
	      end loop;
		  
	 SELECT datname FROM pg_database WHERE datname = 'airflow' into v_database;
	 
	END;  
$body$;

create extension If not exists dblink;
DO
$do$
BEGIN
	raise notice 'Task 02';
   IF EXISTS (SELECT * FROM pg_database WHERE datname = 'airflow') THEN
      RAISE NOTICE 'Database already exists';  -- optional
   ELSE
      PERFORM dblink_exec('dbname=' || current_database()  -- current db
                        , 'CREATE DATABASE airflow owner airflow');
   END IF;
END
$do$;


DO LANGUAGE plpgsql
$body$
DECLARE
    v_user varchar;
BEGIN
	raise notice 'Task 03';
	v_user := 'postgres';
    raise notice 'Changing the password of the user: %', v_user;
	ALTER USER postgres WITH PASSWORD 'postgres';
END;
$body$;

