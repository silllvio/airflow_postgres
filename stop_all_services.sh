#!/bin/bash

# Stop all the containers at once
docker-compose -f dockers/airflow_postgres.yml down

# Stop all services started without compose
docker kill $(docker ps -q)
docker system prune -f